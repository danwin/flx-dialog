/**
 * Modal dialog with buffer for data (Bootstrap's CSS used, w/o plugins), based on promises.
 * Dmitry Zimoglyadov. 2016
 * License: Apache 2.0
 */


// Require promises:
if (typeof Promise === 'undefined') {
	throw new Error('dialog.Window\'s JavaScript requires Promise API or Promise polyfill')
}

var _ = require('underscore');

'use strict';

var dialogs = {};

function DialogWindowFactory () {
    var self = {
        instanceId: null,
        buffer: null,
        _resolve: null,
        _reject: null,
        visible: false
    };

    function makeInstanceId(templateId) {
        return 'instance-'+templateId;
    }

    /*
    Public methods
    */

    /**
     * Create dialog from DOM node <template>
     * @param  {string} templateId DOM id of template element
     * @return {Promise}    Promise for chaining
     */
    self.fromTemplate = function (templateId, settings) {
        // dynamic dialog from template
        var instanceId = makeInstanceId(templateId);
        if (dialogs[instanceId]) {
            return dialogs[instanceId]
        } else {
            self._renderFromTemplate(templateId);
            return self.fromElement(instanceId, settings);
        }
    }

    /**
     * Use DOM element as a body of dialog
     * @param  {string} id DOM id of container element
     * @return {Promise}    Promise for chaining
     */
    self.fromElement = function (id, settings) {
        // static from existing element
        var instanceId = id;
        if (dialogs[instanceId]) {
            return dialogs[instanceId]
        } else {
            if (document.getElementById(instanceId)) {
                self.instanceId = instanceId;
                dialogs[instanceId] = self;
                self.settings = _.extend({
                    renderElVisible: function () {},
                    renderElHidden: function () {},
                }, settings || {});        
                return self;    
            } else {
                throw new Error('Cannot create dialog: element with ID not found: ', id);
            }
        }
    }

    function getElValue(el) {
        try {
            return el.value
        } catch (e) {
            return el.getAttribute('value')
        }
    }

    function setElValue(el, value) {
        try {
            el.value = value
            return true;
        } catch (e) {
            if (el.hasAttribute('value')) {
                el.setAttribute('value', value);
                return true;
            }
        }
    }

    self._processData = function (containerEl, action, keyName, value) {
        var result;
        var possibleSelectors = [
                "input#" + keyName,
                "input[name=" + keyName + "]",
                "textarea#" + keyName,
                "textarea[name=" + keyName + "]",
                "select#" + keyName + " option:checked,select[name=" + keyName + "] option:checked",
                "input[type=checkbox][name=" + keyName + "]:checked",
                "input[type=radio][name=" + keyName + "]:checked"
            ];

        for (var i=0, selector, result; i<possibleSelectors.length; i++) {
            selector = possibleSelectors[i];
            console.log('.setData, ... ', selector);
            containerEl.querySelectorAll(selector).forEach(function(el){
                result = action(el, value);
                console.log('.setData, dynamic ', selector);
            });
            if (typeof result !== 'undefined') return result;
        }
    }

    self.setData = function (data) {
        var containerEl = document.getElementById(self.instanceId), inputsData = _.clone(data);

        // Render static text if any:
        _.each(data, function (value, key) {
            // Allow substituion of elements interior, if any:
            containerEl.querySelectorAll('[data-dialog-value=\"'+key+'\"]').forEach(function(el){
                el.innerHTML = value;
                console.log('.setData, static ', key, value, el);
                delete inputsData[key];
            });
        });

        // Pass data to inputs:
        _.each(inputsData, function (value, key) {
            if (!self._processData(containerEl, setElValue, key, value)) {
                console.error('Cannot find input in dialog for: ', key, value)
            }
        });
    }

    self.getData = function (keys) {
        var containerEl = document.getElementById(self.instanceId);
        var buff = {};
        _.each(keys, function (key) {
            var value = self._processData(containerEl, getElValue, key);
            if (typeof value === undefined) {
                console.error('Cannot find input in dialog for: ', key)
            } else {
                buff[key] = value
            }
        });
        return buff;
    }

    /**
     * Show dialog
     * @param  {object} data Data to pass for dialog controls (initial values). Keys are IDs or names of DOM elements.
     * @return {Promise}          Promise for chaining
     */
    self.show = function (data) {
        var root = document.getElementById(self.instanceId);

        self.buffer = null;

        self._resolve = null;
        self._reject = null;

        // Initial data
        if (data) {
            if (typeof data === 'function') {
                try {
                    data($root);
                } catch (e) {
                    return Promise.reject(e)
                }
            } else if (typeof data === 'object') {
                self.buffer = _.clone(data);
                // Settings in form "domId"/"domName": "value"
                self.setData(self.buffer);
            }
        }

        self._showInterior();
        self.visible = true;

        // Try to focus "default" element:
        root.querySelectorAll('*[autofocus]').forEach(function(el){
            el.focus();
            console.log('element focused', el);
        });

        return new Promise(function (resolve, reject) {
            self._resolve = resolve;
            self._reject = reject;
        });
    }

    /**
     * Hide dialog and resole its promise.  
     * @method close
     * @param  {any} result Modal result. When it is not "truish" or equal to string "Cancel" - dialog returns nothing.
     * @return {undefined} Nothing
     */
    self.close = function (result) {
        var instance, keys;
        // finalize dialog:
        result = (result === "Cancel") ? false : result;
        console.log('closing dialogs, result:', result);
        if (result && self.buffer) {
            instance = document.getElementById(self.instanceId);
            keys = _.keys(self.buffer);
            result = self.getData(keys);
        }
        self._resolve(result);
        self._hideInterior();

        // Clear references
        self._resolve = null;
        self._reject = null;
        self.visible = false;
        delete dialogs[self.instanceId];
    }

    /**
     * Hide dialog and resole its promise. Can pass any value to the pending promise.
     * This method can be used for dialog with multiple exit buttons with different codes
     * @method close
     * @return {[type]} [description]
     */
    self.selectResult = function (result) {
        // finalize dialog:
        self._resolve(result)
        self._hideInterior();
        // clear instance :
        console.log('close called')

        // Clear references
        self._resolve = null;
        self._reject = null;
    }

    /*
    Private methods
        */

    self._showInterior = function () {
        var instance = document.getElementById(self.instanceId);
        self.settings.renderElVisible(instance);
    }

    self._hideInterior = function () {
        var instance = document.getElementById(self.instanceId);
        self.settings.renderElHidden(instance);
    }

    self._renderFromTemplate = function (templateId) {
        var instanceId, instance;

        if (typeof templateId === 'undefined')
            throw new Error('dialogWindow error: templateId is undefined!')
        try {
            var templateHtml = document.getElementById(templateId).innerHTML;
        } catch (e) {
            throw new Error('dialogWindow error: cannot find template element with id: '+templateId)
        }

        instanceId = makeInstanceId(templateId);

        // Check whether instance already exists (error, because we rendeing from template)
        instance = document.getElementById(instanceId);
        if (instance) {
            // clear interior, remove container with previous instances:
            document.removeChild(instance);
            console.warn('dialogWindow: previews instance cleared');
        }

        instance = document.createElement('div');
        instance.setAttribute('id', instanceId);
        instance.innerHTML = templateHtml;
        document.body.appendChild(instance);
    }

    return self;
}

/*
* Install global handlers
*/
function closeVisibleDialogs(modalValue){
    _.each(dialogs, function(dialogObj, instanceId){
        if (dialogObj.visible) {
            dialogObj.close(modalValue);
        }
    })    
}

document.addEventListener('click', function(event) {
    var e = event || window.event, target = e.target || e.srcElement, modalValue;

    if (target.hasAttribute('data-dialog-control')) {
        modalValue = target.getAttribute('data-dialog-control');
        closeVisibleDialogs(modalValue);
    } else if (target.getAttribute('data-dismiss') === "modal") {
        closeVisibleDialogs(false);
    }
});

document.addEventListener('keydown', function(event) {
    event = event || window.event;
    if (event.key === "Escape") {
        closeVisibleDialogs(false);
    }
});

// Export variable:
module.exports = DialogWindowFactory();

