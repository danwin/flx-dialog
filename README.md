# flx-dialog

> Dialog helper on plain js. Depends on underscore, Promise (use polyfill when necessary)

## Install

Install with [npm](https://www.npmjs.com/)

```sh
$ npm i flx-dialog --save-dev
```

## Usage

```js
var dialogWindow = require('flx-dialog');


var dialogOptions = {
    renderElVisible: function (el) {el.removeAttribute('hidden')},
    renderElHidden: function (el) {el.setAttribute('hidden', '')}
}

// TO-DO - move to modal reducer + state2dom reaction on MODAL_SHOW state
dialogWindow.fromElement("watch-form", dialogOptions).show({
    userName: ''
}).then(function (response) {
    if(response) {
        console.log('dialog result: ', response);
        console.log('userName: ', response.userName);
    }
}).catch(function (reason) {
    console.warn('Error changing data ', reason)
});


// Your html for dialog can be like:

<div hidden id="watch-form">

    <button type="button" 
        id="watch-form-close" 
        data-dialog-control="Cancel"></button>

    <input type="text" placeholder="User name ..." name="userName" autofocus="true">

    <button  
        data-target-selector="#watch-form"
        data-dialog-control="Ok"
        >ОК</button>
</div>

// Do not forget to define styles, with visibility rules.
```

## Running tests

Install dev dependencies:

```sh
$ npm i -d && npm test
```

## Contributing

Pull requests and stars are always welcome. For bugs and feature requests, [please create an issue](https://gitlab.com/danwin/flx-dialog/issues)

## Author

**Danwin**

* [github/](https://github.com/danwin)

## License

Copyright © 2018 [Danwin](#Danwin)
Licensed under the ISC license.

***
